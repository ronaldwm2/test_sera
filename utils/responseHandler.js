const responseData = function (values) {
    var data = {
        success: true,
        data: values,
    };
    return data;
};

const responseMessage = function (statusCode, message) {
    var data = {
        success: true,
        message: message,
    };
    return data;
};

const exceptionMessage = function (statusCode, reason) {
    var data = {
        success: false,
        message: reason,
        statusCode: statusCode
    };
    return data;
}

module.exports = { responseData, responseMessage, exceptionMessage };