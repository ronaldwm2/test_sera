Project ini untuk test sera.

<h2>No.1
Logic cara melakukanya</h2>

<p>
Fakta:
 Bola pasti 8
 Action pasti cuma 2
 kelereng 125 pasti cuma 1
 timbangan hanya bisa menimbang 2 sisi
</p>
<p>
Yang ingin dilakukan
 Mencari Kelereng 125 dalam 2x menimbang
</p
<p>
Cara :
 Karena bola pasti 8 dan hanya boleh 2x menimbang artinya bola harus dibagi tiga pada setiap timbangan karena jika dibagi
 dua maka hanya timbangan ketiga yang mendapatkan posisi bola 125

 Contoh:
 Dibagi 3 grup maka akan menjadi
 [125,100,100] [100,100,100] [100,100]

 Grup yang memiliki 3 bola harus kita timbang lalu kita lihat hasilnya.
 Jika hasil grup 1 lebih berat dari grup 2 artinya bola 125 berada pada grup 1.
 Jika hasil grup 2 lebih berat dari grup 1 artinya bola 125 berada pada grup 2.
 Jika hasil grup 1 sama dengan grup 2 artinya bola 125 berada pada grup 3.

 Saat ini kita sudah mendapatkan posisi dimana bola 125 berada yaitu grup 1.

 Lalu setelah kita mendapatkan posisi grup 125 kita lakukan timbangan ke 2 yaitu dengan membandingkan ketiga bola pada grup dimana
 bola 125 berada

 Contoh
 Karena sebelumnya grup 1 maka bola [125,100,100] akan dibandingkan

 lalu kita bandingkan bola pertama dengan bola kedua. 
 Apabila bola pertama lebih berat artinya bola pertama merupakan bola 125
 Apabila bola kedua lebih berat artinya bola kedua merupakan bola 125
 jika sama maka bola ketiga merupakan bola 125.

 Hal ini berlaku untuk case dimana grup 1 2 merupakan tempat bola 125 berada karena terdiri 3 bola.

 Jika terjadi pada grup 3 yaitu hanya 2 bola maka bola hanya perlu dibandingkan saja.
 Jika bola 1 lebih berat maka bola 1 merupakan bola 125.
 Jika bola 2 lebih berat maka bola 1 merupakan bola 125
</p>
<h3><b>link source code nomor 1 (javascript): https://cdn.discordapp.com/attachments/486400953692454915/1075073129786904736/test.js</b></h3>

branch main dan master untuk nomor 2 dan 3 (kedua branch isinya sama)

<b>
DB nya bisa di download disini:
https://cdn.discordapp.com/attachments/486400953692454915/1075072600138592367/test_sera_sql.sql
</b>

Run nya pakai:
node app.js atau nodemon ./app.js

Unit testing:
npm test


Thankyou.