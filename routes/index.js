var express = require('express');
const { getAllUser, createNewUser, userLogin, editUser, deleteUser, getSpecificUser } = require('../models/User.model');
const { responseData, responseMessage, exceptionMessage } = require('../utils/responseHandler');
var router = express.Router();

/* GET user. */
router.get('/getAllUser', async function(req, res, next) {
  try {
    const userData = await getAllUser();
    res.send(userData);
  } catch (e) {
    res.status(e.statusCode);
    res.send(e);
  }
});

router.get('/getSpecificUser', async function(req, res, next) {
  try {
    let id = req.query.id;
    const userData = await getSpecificUser(id);
    res.send(userData);
  } catch (e) {
    res.status(e.statusCode);
    res.send(e);
  }
});


/* create new user */
router.post('/createNewUser', async function(req, res, next) {
  try {
    const body = req.body;
    if (!body.password || body.password == "") {
      const errM = exceptionMessage(500, "Password should be defined");
      throw errM;
    }
    if (body.password.length < 5) {
      const errM = exceptionMessage(500, "Password length should be more than 5");
      throw errM;
    }
    if (body.password.length > 20) {
      const errM = exceptionMessage(500, "Password length cannot more than 20");
      throw errM;
    }
    if (!body.id) {
      const errM = exceptionMessage(500, "Please define ID");
      throw errM;
    }
    if ((/[!@#$%^&*()_+\-=\[\]{};':"\\|,<>\/?]+/).test(body.id)) {
      const errM = exceptionMessage(500, "ID contain special characters");
      throw errM;
    }

    const userData = await createNewUser(body.id, body.password);
    res.send(userData);
  } catch (e) {
    res.status(e.statusCode);
    res.send(e);
  }
});

router.post('/login', async function(req, res, next) {
  try {
    const body = req.body;
    if (!body.password || body.password == "") {
      const errM = exceptionMessage(500, "Password should be defined");
      throw errM;
    }
    if (!body.id) {
      const errM = exceptionMessage(500, "Please define ID");
      throw errM;
    }
    if ((/[!@#$%^&*()_+\-=\[\]{};':"\\|,<>\/?]+/).test(body.id)) {
      const errM = exceptionMessage(500, "ID contain special characters");
      throw errM;
    }
    const userData = await userLogin(body.id, body.password);
    res.send(userData);
  } catch (e) {
    res.status(e.statusCode);
    res.send(e);
  }
});

router.post('/editProfile', async function(req, res, next) {
  try {
    const body = req.body;
    if (!body.address) {
      body.address = "";
    }
    const userToken = req.headers.authorization || req.headers.Authorization;
    if (!userToken) {
      const errM = exceptionMessage(500, "Unknown Authorization");
      throw errM;
    }
    const address = body.address;

    const userData = await editUser(userToken, address);
    res.send(userData);
  } catch (e) {
    res.status(e.statusCode);
    res.send(e);
  }
})

router.post('/deleteUsers', async function(req, res, next) {
  try {
    const body = req.body;
    if (!body.id) {
      const errM = exceptionMessage(500, "Please define ID");
      throw errM;
    }

    const userData = await deleteUser(body.id);
    res.send(userData);
  } catch (e) {
    res.status(e.statusCode);
    res.send(e);
  }
})


module.exports = router;
