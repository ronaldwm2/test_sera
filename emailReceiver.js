var amqp = require('amqplib/callback_api');
const nodemailer = require('nodemailer');
require('dotenv').config()
let transporter;

try{
    transporter = nodemailer.createTransport({
        host: process.env.mail_host,
        port: process.env.mail_port,
        secure: true,
        auth: {
            user: process.env.mail_username,
            pass: process.env.mail_password,
        },
    });
} catch (e) {
    console.log(e);
}


exports.checkEmailAuth = async () => {
    return new Promise((resolve) => {
        transporter.verify().then((val) => {
            resolve(true);
        }, (err) => {
            resolve(false);
        })
    })


}

amqp.connect('amqp://localhost', function(error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }

        var queue = 'emailQueue';

        channel.assertQueue(queue, {
            durable: false
        });

        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);

        channel.consume(queue, function(msg) {
            console.log(" [x] Received %s", msg.content.toString());
            
            var mailOptions = {
                from: `"testing" <testsera@gmail.com>`,
                to: 'ronald.marbun@gmail.com',
                subject: msg.content.toString(),
                text: msg.content.toString(),
                html: msg.content.toString()
            }

            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.log(error);
                }
                console.log("send email to ronald.marbun@gmail.com");
            })
        }, {
            noAck: true
        });
    });
});