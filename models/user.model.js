const mysqlConn = require('./../database-config');
const { responseData, responseMessage, exceptionMessage } = require('../utils/responseHandler');
var md5 = require('md5');
const { v4:uuidv4 } = require('uuid');

var amqp = require('amqplib/callback_api');

exports.sendEmailQueue = (messages) => {
    amqp.connect('amqp://localhost', function(error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function(error1, channel) {
            if (error1) {
                throw error1;
            }
    
            var queue = 'emailQueue';
            var msg = messages;
    
            channel.assertQueue(queue, {
                durable: false
            });
            channel.sendToQueue(queue, Buffer.from(msg));
    
            console.log(" [x] Sent %s", msg);
        });
        setTimeout(function() {
            connection.close();
        }, 500);
    });
}

exports.getAllUser = () => {
    return new Promise((resolve, reject) => {
        mysqlConn.query(`
            SELECT user.username, user_detail.address
            FROM user
            LEFT JOIN user_detail ON user_detail.username = user.username`, 
            (err, result, fields) => {
                if (err) {
                    let responseError = exceptionMessage(500, err.message);
                    reject(responseError);
                } else {
                    resolve(responseData(result));
                }
            })
    })
}

exports.getSpecificUser = (userId) => {
    return new Promise((resolve, reject) => {
        let sanitizedId = mysqlConn.escape(userId);
        mysqlConn.query(`
            SELECT user.username, user_detail.address
            FROM user
            LEFT JOIN user_detail ON user_detail.username = user.username
            WHERE user.username = ${sanitizedId}`, 
            (err, result, fields) => {
                if (err) {
                    let responseError = exceptionMessage(500, err.message);
                    reject(responseError);
                } else {
                    if (result.length == 0) {
                        let responseError = exceptionMessage(500, "User Not Found");
                        reject(responseError);
                    }
                    resolve(responseData(result[0]));
                }
            })
    })
}

exports.deleteUser = (id) => {
    return new Promise(async (resolve, reject) => {
        const sanitizedId = mysqlConn.escape(id);
        await mysqlConn.beginTransaction();

        let queries = `DELETE FROM user_detail WHERE username = ${sanitizedId}`;

        mysqlConn.query(queries, (err, result, fields) => {
            if (err) {
                let responseError = exceptionMessage(500, err.message);
                mysqlConn.rollback();
                reject (responseError);
            }
            queries = `DELETE FROM user WHERE username = ${sanitizedId}`;
            mysqlConn.query(queries, (err, result, fields) => {
                if (err) {
                    let responseError = exceptionMessage(500, err.message);
                    mysqlConn.rollback();
                    reject (responseError);
                }
                resolve(responseMessage(200, "Success delete user"));
            })
        })
    })
}

exports.editUser = (userToken, address) => {
    return new Promise(async (resolve, reject) => {
        const sanitizedAddress = mysqlConn.escape(address);
        const sanitizedToken = mysqlConn.escape(userToken);
        let getUserFromToken;
        try {
            getUserFromToken = await new Promise((resolve, reject) => {
                const queries = `SELECT username FROM user_token WHERE token_id = ${sanitizedToken}`;
                mysqlConn.query(queries, (err, result, fields) => {
                    if (err) {
                        let responseError = exceptionMessage(500, err.message);
                        reject(responseError);
                    } else {
                        if (result.length == 0) {
                            let responseError = exceptionMessage(500, "Unknown Token or Expired!");
                            reject(responseError);
                        } else {
                            resolve(result[0].username);
                            
                        }
                    }
                })
            })
        } catch (e) {
            reject(e);
        }
        
        const sanitizedId = mysqlConn.escape(getUserFromToken);

        const queries = `UPDATE user_detail SET address = ${sanitizedAddress} WHERE username = ${sanitizedId}`;
        mysqlConn.query(queries, (err, result, fields) => {
            if (err) {
                let responseError = exceptionMessage(500, err.message);
                reject(responseError);
            } else {
                resolve(responseMessage(200, "Success edit user"));
            }
        })
    })
}

exports.userLogin = (id, password) => {
    return new Promise(async (resolve, reject) => {
        try {
            const sanitizedId = mysqlConn.escape(id);
            let queries = `
                SELECT username, password, salt FROM user WHERE username = ${sanitizedId};
            `;
            await mysqlConn.beginTransaction();
            mysqlConn.query(queries, (err, result, fields) => {
                if (err) {
                    let responseError = exceptionMessage(500, err.message);
                    mysqlConn.rollback();
                    reject(responseError);
                } else {
                    if (result.length == 0) {
                        let responseError = exceptionMessage(500, "Username or password is wrong, Please Check");
                        mysqlConn.rollback();
                        reject(responseError);
                    } else {
                        const userObject = result[0];
                        const passwordAndSalt = userObject.salt + password;
                        const hashedPassword = md5(passwordAndSalt);
                        const sanitizedPassword = mysqlConn.escape(hashedPassword);

                        queries = `
                            SELECT username, password, salt FROM user WHERE username = ${sanitizedId} AND password = ${sanitizedPassword};
                        `;
                        mysqlConn.query(queries, (err, result, fields) => {
                            if (err) {
                                let responseError = exceptionMessage(500, err.message);
                                mysqlConn.rollback();
                                reject(responseError);
                            }
                            if (result.length == 0) {
                                let responseError = exceptionMessage(500, "Username or password is wrong, Please Check");
                                mysqlConn.rollback();
                                reject(responseError);
                            } else {
                                let loginInfo = {
                                    id: result[0].username,
                                    token: uuidv4()
                                }

                                const sanitizedToken = mysqlConn.escape(loginInfo.token);

                                queries = `INSERT INTO user_token (token_id, username) VALUES (${sanitizedToken}, ${sanitizedId})`;
                                mysqlConn.query(queries, (err, result, fields) => {
                                    if (err) {
                                        let responseError = exceptionMessage(500, err.message);
                                        mysqlConn.rollback();
                                        reject(responseError);
                                    }
                                    mysqlConn.commit();
                                    resolve(responseData(loginInfo));
                                })
                                // resolve(responseData(loginInfo));
                            }
                        });
                    }
                }
            })
        } catch (err) {
            let responseError = exceptionMessage(500, err.message);
            reject(responseError);
        }
    })
}

exports.createNewUser = (id, password) => {
    return new Promise(async (resolve, reject) => {
        try {
            const salt = uuidv4();
            const passwordAndSalt = salt + password;
            const hashedPassword = md5(passwordAndSalt);

            const sanitizedId = mysqlConn.escape(id);
            const sanitizedPassword = mysqlConn.escape(hashedPassword);
            const sanitizedSalt = mysqlConn.escape(salt);

            await mysqlConn.beginTransaction();

            let queries = `
                INSERT INTO user (username, password, salt) VALUES (${sanitizedId}, ${sanitizedPassword}, ${sanitizedSalt});
            `;

            mysqlConn.query(queries, (err, result, fields) => {
                if (err) {
                    let responseError = exceptionMessage(500, err.message);
                    if (err.errno == 1062) {
                        responseError.message = "Account already created!"
                    }
                    mysqlConn.rollback();
                    reject(responseError);
                } else {
                    queries = `INSERT INTO user_detail (username) VALUES (${sanitizedId})`;
                    mysqlConn.query(queries, (err, result, fields) => {
                        if (err) {
                            let responseError = exceptionMessage(500, err.message);
                            mysqlConn.rollback();
                            reject(responseError);
                        }
                        this.sendEmailQueue("Thanks for registering with us");
                        mysqlConn.commit();
                        resolve(responseMessage(200, "Success creating new user"));
                    })
                    
                }
            })
        } catch (err) {
            let responseError = exceptionMessage(500, err.message);
            reject(responseError);
        }

    })
}