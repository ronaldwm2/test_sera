const chai = require('chai');
const chaiHttp = require('chai-http');
var logger = require('morgan');
chai.use(chaiHttp);
const mysqlConn = require('./../database-config');
const { checkEmailAuth } = require('../emailReceiver');
const userModel = require('../models/user.model');

var expect = chai.expect;
const app = require('../app');
const sinon = require('sinon');

describe("Email Testing", function() {
    it("Authorization True", async function() {
        let checkRes = await checkEmailAuth();

        expect(checkRes).to.equal(true);
    })
})

describe("User Endpoint", function() {
    sinon.stub(console);
    sinon.stub(logger);

    before(async () => {
        return new Promise((resolve) => {
            mysqlConn.query(`DELETE FROM user`,  (err, result, fields) => {
                mysqlConn.query(`DELETE FROM user_detail`,  (err, result, fields) => {
                    mysqlConn.query(`DELETE FROM user_token`,  (err, result, fields) => {
                        resolve()
                    });
                });
            });
            
            
        })
    })


    let loggedInStatus = [];
    describe("Main Function Testing", function() {
        let emailStub = sinon.stub(userModel, "sendEmailQueue");

        it("Get All Users (Current data empty)", function() {
            chai.request(app)
                .get("/getAllUser")
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    expect(res.body.data).to.be.an("array");
                    expect(res.body.success).to.equal(true);
                    expect(res.body.data).to.have.lengthOf(0);
                })
        });
    
        it("Create new user", async function() {
            const payload = {
                "id": "ronaldwm",
                "password": "pepehands"
            }

            const data = await userModel.createNewUser(payload.id, payload.password);
            expect(emailStub.callCount).to.equal(1);
            expect(data).to.be.an("object");
            expect(data.success).to.equal(true);
        });
    
        it("Get Newly Created User", function() {
            chai.request(app)
                .get("/getSpecificUser")
                .query({
                    id: "ronaldwm"
                })
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    expect(res.body.data.username).to.equal("ronaldwm");
                    expect(res.body.success).to.equal(true);
                })
        });
    
        it("Create duplicate user", function() {
            const payload = {
                "id": "ronaldwm",
                "password": "pepehands"
            }
            chai.request(app)
                .post("/createNewUser")
                .set('content-type', 'application/json')
                .send(payload)
                .end((err, res) => {
                    expect(res.body).to.be.an("object");
                    expect(res.status).to.equal(500);
                    expect(res.body.success).to.equal(false);
                })
        });
    
        it("Create second user", function() {
            const payload = {
                "id": "ronaldwms",
                "password": "pepehands"
            }
    
            chai.request(app)
                .post("/createNewUser")
                .set('content-type', 'application/json')
                .send(payload)
                .end((err, res) => {
                    expect(res.body).to.be.an("object");
                    expect(res.body.success).to.equal(true);
                })
        });
    
        
        it("Get All Users With 2 data", function() {
            chai.request(app)
                .get("/getAllUser")
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    expect(res.body.data).to.be.an("array");
                    expect(res.body.success).to.equal(true);
                    expect(res.body.data).to.have.lengthOf(2);
                })
        });
    
    
        it("Delete second user", function() {
            const payload = {
                "id": "ronaldwms"
            }
    
            chai.request(app)
                .post("/deleteUsers")
                .set('content-type', 'application/json')
                .send(payload)
                .end((err, res) => {
                    chai.request(app)
                        .get("/getAllUser")
                        .end((err, res) => {
                            expect(res.body.data).to.be.an("array");
                            expect(res.body.data).to.have.lengthOf(1);
                        })
                })
        });
    
        it("Second User (deleted user) Login", function() {
            const payload = {
                "id": "ronaldwms",
                "password": "pepehands"
            }
    
            chai.request(app)
                .post("/login")
                .set('content-type', 'application/json')
                .send(payload)
                .end((err, res) => {
                    expect(res.body).to.be.an("object");
                    expect(res.body.success).to.equal(false);
                    expect(res.status).to.equal(500);
                })
        });
    
        it("First User Login", function() {
            const payload = {
                "id": "ronaldwm",
                "password": "pepehands"
            }
    
            chai.request(app)
                .post("/login")
                .set('content-type', 'application/json')
                .send(payload)
                .end((err, res) => {
                    expect(res.body).to.be.an("object");
                    expect(res.body.success).to.equal(true);
                    expect(res.status).to.equal(200);
                    loggedInStatus.push(res.body.data)
                })
        });
    
        it("Create Third user", function() {
            const payload = {
                "id": "ronaldwmss",
                "password": "pepehands"
            }
    
            chai.request(app)
                .post("/createNewUser")
                .set('content-type', 'application/json')
                .send(payload)
                .end((err, res) => {
                    expect(res.body).to.be.an("object");
                    expect(res.body.success).to.equal(true);
                })
        });
    
        it("Third User Login", async function() {
            return new Promise((resolve) => {
                const payload = {
                    "id": "ronaldwmss",
                    "password": "pepehands"
                }
        
                chai.request(app)
                    .post("/login")
                    .set('content-type', 'application/json')
                    .send(payload)
                    .end((err, res) => {
                        expect(res.body).to.be.an("object");
                        expect(res.body.success).to.equal(true);
                        expect(res.status).to.equal(200);
                        loggedInStatus.push(res.body.data);
                        resolve();
                    })
            })
    
        });
    
        it("First User Update Data", function() {
            const payload = {
                "address": "Rumah 1"
            }
            chai.request(app)
                .post("/editProfile")
                .set('content-type', 'application/json')
                .set('Authorization', loggedInStatus[0].token)
                .send(payload)
                .end((err, res) => {
                    chai.request(app)
                        .get("/getSpecificUser")
                        .query({
                            id: loggedInStatus[0].id
                        })
                        .end((err, res) => {
                            expect(res.status).to.equal(200);
                            expect(res.body.data.username).to.equal("ronaldwm");
                            expect(res.body.data.address).to.equal("Rumah 1");
                            expect(res.body.success).to.equal(true);
                        })
                })
        });
    
        it("Third User Update Data", function() {
            const payload = {
                "address": "Rumah 3"
            }
            chai.request(app)
                .post("/editProfile")
                .set('content-type', 'application/json')
                .set('Authorization', loggedInStatus[1].token)
                .send(payload)
                .end((err, res) => {
                    chai.request(app)
                        .get("/getSpecificUser")
                        .query({
                            id: loggedInStatus[1].id
                        })
                        .end((err, res) => {
                            expect(res.status).to.equal(200);
                            expect(res.body.data.username).to.equal("ronaldwmss");
                            expect(res.body.data.address).to.equal("Rumah 3");
                            expect(res.body.success).to.equal(true);
                        })
                })
        });
    })

    describe("Input Testing", function() {
        it("Create new user with id special character", function() {
            const payload = {
                "id": "ronaldwm%!",
                "password": "pepehands"
            }
            chai.request(app)
                .post("/createNewUser")
                .set('content-type', 'application/json')
                .send(payload)
                .end((err, res) => {
                    expect(res.body.message).to.equal("ID contain special characters");
                    expect(res.body).to.be.an("object");
                    expect(res.body.success).to.equal(false);
                    expect(res.status).to.equal(500);
                })
        });
        it("Create new user without password param", function() {
            const payload = {
                "id": "ronaldwm"
            }
            chai.request(app)
                .post("/createNewUser")
                .set('content-type', 'application/json')
                .send(payload)
                .end((err, res) => {
                    expect(res.body.message).to.equal("Password should be defined");
                    expect(res.body).to.be.an("object");
                    expect(res.body.success).to.equal(false);
                    expect(res.status).to.equal(500);
                })
        });

        it("Create new user with password length < 5", function() {
            const payload = {
                "id": "ronaldwm",
                "password": "a"
            }
            chai.request(app)
                .post("/createNewUser")
                .set('content-type', 'application/json')
                .send(payload)
                .end((err, res) => {
                    expect(res.body.message).to.equal("Password length should be more than 5");
                    expect(res.body).to.be.an("object");
                    expect(res.body.success).to.equal(false);
                    expect(res.status).to.equal(500);
                })
        });

        it("Create new user with password length > 20", function() {
            const payload = {
                "id": "ronaldwm",
                "password": "123456789012345678901"
            }
            chai.request(app)
                .post("/createNewUser")
                .set('content-type', 'application/json')
                .send(payload)
                .end((err, res) => {
                    expect(res.body.message).to.equal("Password length cannot more than 20");
                    expect(res.body).to.be.an("object");
                    expect(res.body.success).to.equal(false);
                    expect(res.status).to.equal(500);
                })
        });


        it("Create new user with password param empty", function() {
            const payload = {
                "id": "ronaldwm%!",
                "password": ""
            }
            chai.request(app)
                .post("/createNewUser")
                .set('content-type', 'application/json')
                .send(payload)
                .end((err, res) => {
                    expect(res.body.message).to.equal("Password should be defined");
                    expect(res.body).to.be.an("object");
                    expect(res.body.success).to.equal(false);
                    expect(res.status).to.equal(500);
                })
        });

        it("Create new user without id param", function() {
            const payload = {
                "password": "pepehands"
            }
            chai.request(app)
                .post("/createNewUser")
                .set('content-type', 'application/json')
                .send(payload)
                .end((err, res) => {
                    expect(res.body.message).to.equal("Please define ID");
                    expect(res.body).to.be.an("object");
                    expect(res.body.success).to.equal(false);
                    expect(res.status).to.equal(500);
                })
        });

        it("Get Unknown User", function() {
            const payload = {
                "id": "ronaldwmsss"
            }
            chai.request(app)
                .get("/getSpecificUser")
                .query(payload)
                .end((err, res) => {
                    expect(res.body.message).to.equal("User Not Found");
                    expect(res.status).to.equal(500);
                    expect(res.body.success).to.equal(false);
                })
        });

        it("Login without id", function() {
            const payload = {
                "password": "pepehands"
            }
            chai.request(app)
                .post("/login")
                .set('content-type', 'application/json')
                .send(payload)
                .end((err, res) => {
                    expect(res.body.message).to.equal("Please define ID");
                    expect(res.body).to.be.an("object");
                    expect(res.body.success).to.equal(false);
                    expect(res.status).to.equal(500);
                })
        });

        it("Login without password", function() {
            const payload = {
                "id": "ronaldwm"
            }
            chai.request(app)
                .post("/login")
                .set('content-type', 'application/json')
                .send(payload)
                .end((err, res) => {
                    expect(res.body.message).to.equal("Password should be defined");
                    expect(res.body).to.be.an("object");
                    expect(res.body.success).to.equal(false);
                    expect(res.status).to.equal(500);
                })
        });

        it("Login with special characters !%", function() {
            const payload = {
                "id": "ronaldwm!%",
                "password": "pepehands"
            }
            chai.request(app)
                .post("/login")
                .set('content-type', 'application/json')
                .send(payload)
                .end((err, res) => {
                    expect(res.body.message).to.equal("ID contain special characters");
                    expect(res.body).to.be.an("object");
                    expect(res.body.success).to.equal(false);
                    expect(res.status).to.equal(500);
                })
        });

        it("Login with special characters !", function() {
            const payload = {
                "id": "ronaldwm!",
                "password": "pepehands"
            }
            chai.request(app)
                .post("/login")
                .set('content-type', 'application/json')
                .send(payload)
                .end((err, res) => {
                    expect(res.body).to.be.an("object");
                    expect(res.body.success).to.equal(false);
                    expect(res.body.message).to.equal("ID contain special characters");
                    expect(res.status).to.equal(500);
                })
        });

        it("Change address without token", function() {
            const payload = {
                "address": "Rumah 5"
            }
            chai.request(app)
                .post("/editProfile")
                .set('content-type', 'application/json')
                .send(payload)
                .end((err, res) => {
                    expect(res.body.success).to.equal(false);
                    expect(res.body.message).to.equal("Unknown Authorization");
                    expect(res.status).to.equal(500);
                })
        })

        it("Delete without id", function() {
            const payload = {
            }
            chai.request(app)
                .post("/deleteUsers")
                .set('content-type', 'application/json')
                .send(payload)
                .end((err, res) => {
                    expect(res.body.success).to.equal(false);
                    expect(res.body.message).to.equal("Please define ID");
                    expect(res.status).to.equal(500);
                })
        })
    })


});