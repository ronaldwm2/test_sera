const mysql = require('mysql');
require('dotenv').config()

const mysqlConn = mysql.createConnection({
    host: process.env.host,
    user: process.env.user,
    password: process.env.password,
    database: process.env.database
});

mysqlConn.connect((err) => {
    if (err) throw err;
    console.log(`Connected To ${mysqlConn.config.host}`);
})

module.exports = mysqlConn;